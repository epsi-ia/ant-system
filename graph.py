import csv
import networkx as nx
import matplotlib.pyplot as plt

def get_base_graph(ants=1):
	'''Permet de récupérer le graphe issue
	des données fournies dans le fichier CSV
	'''
	G = nx.Graph()

	# Lecture des données du CSV et création des edges
	# avec leurs nodes.
	with open('VOIES_NM.csv') as csvfile:
		reader = csv.DictReader(csvfile)

		for row in reader:
			if (row['COMMUNE'] == 'BASSE-GOULAINE' and (row['TENANT'] or row['ABOUTISSANT'])):
				tenant = row['TENANT']
				aboutissant = row['ABOUTISSANT']
				libelle = row['LIBELLE_CONCAT']
				# To avoid division by 0, we set default length to 1
				length = 1 if not row['BI_MAX'] else int(row['BI_MAX'])

				# impasse tenant or aboutissant empty (or aboutissant is "Impasse")
				# it's a final state
				if((aboutissant == 'Impasse' or aboutissant == '') or (aboutissant == tenant)):
					G.add_edge(tenant, libelle, street = libelle, length = length)
				elif(tenant == ''):
					G.add_edge(libelle, aboutissant, street = libelle, length = length)
				else:
					G.add_edge(tenant, aboutissant, street = libelle, length = length)

	t0 = ants / len(G.nodes()) # initial pheromones value

	for e in G.edges(data=True):
		e[2]["pheromones"] = t0

	return G


def build_graph(G, nodes):
	'''Création d'un graphe à partir de données
	d'un autre graphe. Utilisé pour la génération
	du graphe d'un chemin.
	'''
	graph = nx.Graph()

	for i in range(0, len(nodes)):
		curr_node = nodes[i]

		# récupération des edges à partir des nodes
		if(i+1 < len(nodes)):
			next_node = nodes[i+1]
			curr_edge = G[curr_node][next_node]
			graph.add_edge(
				curr_node,
				next_node, street = curr_edge["street"],
				length = curr_edge["length"],
				pheromones = curr_edge["pheromones"]
			)
			curr_node = next_node

	return graph


def get_edges(G, nodes):
	'''Permet de récupérer les edges à partir
	des nodes donnés
	'''
	edges = []

	for i in range(0, len(nodes)):
		curr_node = nodes[i]

		if(i+1 < len(nodes)):
			next_node = nodes[i+1]
			edges.append(G[curr_node][next_node])

	return edges


def get_edge(G, street):
	'''Permet de récupérer un edge
	à partir du nom d'une rue
	'''
	edges = G.edges(data=True)

	for e in edges:
		if(e[2]['street'] == street):
			return e

	return None


def get_street(edge):
	'''Permet de récupérer une rue
	à partir d'un edge donné
	'''
	return edge[2]['street']

def draw(G):
	'''Fonction permettant de dessiner
	un graphe donné.
	'''
	pos = nx.spring_layout(G)
	nx.draw(G, pos)

	edge_labels= dict([((u,v,),d['street'] + "," + str(d['length']))
            for u,v,d in G.edges(data=True)])

	nx.draw_networkx_edge_labels(G, pos, edge_labels = edge_labels)
	plt.show()
