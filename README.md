# General
- python 3.5

# Quickstart
 - `pip install networkx`
 - `python shortest_path.py`

# Graph visualization (very user friendly)

- the first one is the graph concerning BASSE-GOULAINE
- you must close the first one to see the solution graph

## How to read
`node ---- edge(street,length) ---- node`
