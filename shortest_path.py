import networkx as nx
import random
import graph
import math
from numpy.random import choice

M = 1000 # number of ants
G = graph.get_base_graph(M) # graph (will set initial pheromones value)
NODES = G.nodes() # graph nodes
A = 1 # intensity influence
B = 2 # visibility influence
P = 0.5 # evaporation of pheromones
Q = 1 # pheromone parameter for update


def fitness(n):
	'''Fonction de fitness
	basée sur la distance et l'intensité
	d'une piste.

	n : le voisin composé d'un node et d'un edge
	'''
	length = n[1]['length']
	pheromones = n[1]['pheromones']
	score = math.pow(1/length, B) * math.pow(pheromones, A)

	return score


def apply_fitness(neighbors):
	'''Application du calcul de la fonction de
	fitness à chaque voisin d'un ensemble donné.
	Retourne un tableau de voisins et un tableau de score associés.

	neighbors : la liste des voisins à traiter
	'''
	total = 0
	scores = []
	neighbors_1_dim = []

	for n in neighbors:
		total += fitness(n)

	for n in neighbors:
		neighbors_1_dim.append(n[0])
		scores.append(fitness(n) / total)

	return neighbors_1_dim, scores


def update_pheromones(G, nodes):
	'''Mise à jour de la quantité de phéromones
	pour les edges récupérés à partir d'une liste
	de nodes.

	G: le graphe
	nodes: les noeuds du graphe
	'''
	total_length = 1 # avoid division by zero
	edges = graph.get_edges(G, nodes)

	for e in edges:
		total_length += e["length"]

	for e in edges:
		e["pheromones"] += Q / total_length


def pheromones_evaporation(G, excluded_edges):
	'''Evaporation des phéromones uniquement pour les edges
	des itérations précédentes.

	G: Le graphe concerné par la mise à jour
	excluded_edges: les arcs à ne pas évaporer car issue de la dernière
					itéaration
	'''
	edges = G.edges(data=True)
	for e in edges:
		if e not in excluded_edges:
			pheromones = e[2]["pheromones"]

			if (pheromones - (P * pheromones) > 0):
				e[2]["pheromones"] -= (P * pheromones)
			else:
				e[2]["pheromones"] = 0


def get_valid_neighbors(G, node, explored_nodes, real_path):
	'''Récupération des voisins explorables
	Tant qu'il n'y a pas de solution on revient en
	arrière (backtracking).

	G: le graphe
	node: Le noeud pour lequel on souhaite récupérer les voisins
	explored_nodes: La liste des noeuds explorés
	real_path: Le chemin retenu après backtracking
	'''
	neighbors = G[node]
	valid_neighbors = []

	for n in neighbors.items():
		if(n[0] not in explored_nodes):
			valid_neighbors.append(n)

	# if list is empty : backtracking
	if not valid_neighbors:
		if(len(real_path) > 1):
			last = real_path.pop()
		else:
			return valid_neighbors

		# check case where u are at the start
		previous_node = real_path[-1]
		valid_neighbors = get_valid_neighbors(G, previous_node, explored_nodes, real_path)

	return valid_neighbors


def explore_graph(G, start, stop):
	'''Permet d'explorer le graphe par un déplacement
	de noeud en noeud à partir d'un noeud de départ
	jusqu'à un noeud d'arrivée.

	G: Le graphe
	start: Le noeud de départ
	stop: Le noeud d'arrivée
	'''

	current_node = start
	explored_nodes = [current_node]
	path = [current_node]
	found = False

	while(not found):
		# Get explorable neighbor nodes or use backward
		valid_neighbors = get_valid_neighbors(G, current_node, explored_nodes, path)

		if(not valid_neighbors):
			print("NO SOLUTION FOUND for ant N°" + str(ant))
			break

		neighbors_1_dim, scores = apply_fitness(valid_neighbors)
		rand_neighbor = choice(neighbors_1_dim, 1, p=scores)
		neighbor = valid_neighbors[neighbors_1_dim.index(rand_neighbor)]
		current_node = neighbor[0]
		explored_nodes.append(current_node)
		path.append(current_node)

		if(current_node == stop):
			found = True

	update_pheromones(G, path)

	return explored_nodes, path

# main program
start = "BASSE-GOULAINE Impasse du Muguet"
stop = "BASSE-GOULAINE Rue de Provence"

if(start not in NODES):
	print(start + " not found in nodes")

if(stop not in NODES):
	print(stop + " not found in nodes")

ants_paths = []

if(start in NODES and stop in NODES):
	# M the number of ants
	for ant in range(0, M):
		explored_nodes, path = explore_graph(G, start, stop)

		# construct a solution
		path_graph = graph.build_graph(G, path)
		path_edges = path_graph.edges(data=True)

		# pheromones evaporation
		pheromones_evaporation(G, path_edges)

		# compute the total distance
		total_length = 1
		for e in path_edges:
			total_length += e[2]["length"]

		# save the solution
		ants_paths.append({"graph": path_graph, "path": path, "distance": total_length})

# choose the best
solution = ants_paths[0]

for ant_path in ants_paths[1:]:
	# one of the bests
	# We can have multiple solutions with the same distance...
	if(ant_path["distance"] < solution["distance"]):
		solution = ant_path

print("ONE OF THE BEST SOLUTIONS (based on the distance) :")
print("=========== NODES ===========")
for node in solution["path"]:
	print(node)

print("=========== TOTAL DISTANCE ===========")
print(solution["distance"])

# Draw the full graph before
graph.draw(G)

# Draw the solution
graph.draw(solution["graph"])
